package types;

public enum ShapeTypes {
    HEXAGON,
    STAR,
    SQUARE,
    TRIANGLE,
    PACMAN
}
