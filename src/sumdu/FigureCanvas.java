package sumdu;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

public class FigureCanvas {
    private boolean isDone;
    private int startAngle;
    private Figure figure;
    private Graphics2D graphics2d;
    private int width;
    private int height;

    public FigureCanvas(Figure figure) {
        this.figure = figure;
    }

    public void draw() {
        isDone = false;

        initGraphics();
        fillWithFigures();
        updateStartAngle();

        isDone = true;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setGraphics2d(Graphics2D graphics2d) {
        this.graphics2d = graphics2d;
    }

    private void initGraphics() {
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2d.setStroke(figure.getStroke());
        graphics2d.setPaint(figure.getPaint());
    }

    private void fillWithFigures() {
        double angle = startAngle;
        final double dr = 90.0 * 1.5 * figure.getWidth() / width;
        for (int j = figure
            .getHeight(); j < height; j += (int) (figure.getHeight() * 1.5)) {
            for (int i = figure
                .getWidth(); i < width; i += (int) (figure.getWidth() * 1.5)) {
                angle = generateNewAngle(angle, dr);
                drawFigureAt(angle, j, i);
            }
        }
    }

    private static double generateNewAngle(double oldAngle, double dr) {
        return (oldAngle > 360.0) ? 0.0 : (oldAngle + dr);
    }

    private void drawFigureAt(double angle, int y, int x) {
        final AffineTransform transform = new AffineTransform();
        transform.translate(x, y);
        transform.rotate(Math.toRadians(angle));
        graphics2d.draw(transform.createTransformedShape(figure.getShape()));
    }

    private void updateStartAngle() {
        startAngle++;
        if (startAngle > 360) {
            startAngle = 0;
        }
    }
}
