package sumdu;

import java.awt.geom.Point2D;
import java.awt.GradientPaint;
import java.awt.Color;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

import types.ColorTypes;
import types.ShapeTypes;
import types.StrokeTypes;

import java.awt.Point;
import java.awt.Paint;
import java.awt.BasicStroke;
import java.awt.Shape;

// 
// Decompiled by Procyon v0.5.36
// 

public class Figure {
    private static final String TYPE_IS_UNSUPPORTED = "type is unsupported";

    private Shape shape;
    private BasicStroke stroke;
    private Paint paint;
    private int width;
    private int height;

    private Map<ShapeTypes, ShapeProvider> shapeFactory;
    private Map<StrokeTypes, StrokeProvider> strokeFactory;
    private Map<ColorTypes, ColorProvider> colorFactory;
    
    {
        shapeFactory = new EnumMap<>(ShapeTypes.class);
        strokeFactory = new EnumMap<>(StrokeTypes.class);
        colorFactory = new EnumMap<>(ColorTypes.class);

        shapeFactory.put(ShapeTypes.HEXAGON,
                () -> createStar(3, new Point(0, 0), width / 2.0, width / 2.0));
        shapeFactory.put(ShapeTypes.STAR,
                () -> createStar(5, new Point(0, 0), width / 2.0, width / 4.0));
        shapeFactory.put(ShapeTypes.SQUARE,
                () -> new Rectangle2D.Double(-width / 2.0, -height / 2.0, width,
                        height));
        shapeFactory.put(ShapeTypes.TRIANGLE,
                () -> createTriangle(height, width));

        strokeFactory.put(StrokeTypes.THIN, () -> new BasicStroke(3.0f));
        strokeFactory.put(StrokeTypes.THICK, () -> new BasicStroke(7.0f));

        colorFactory.put(ColorTypes.BLACK, () -> Color.BLACK);
        colorFactory.put(ColorTypes.RED, () -> Color.RED);
        colorFactory.put(ColorTypes.GRADIENT, () -> new GradientPaint(-width,
                -height, Color.white, width, height, Color.gray, true));
    }

    public Figure() {
        width = 25;
        height = 25;
        setShape(ShapeTypes.HEXAGON);
        setStroke(StrokeTypes.THIN);
        setColor(ColorTypes.BLACK);
    }

    public void setShape(final ShapeTypes type) {
        ShapeProvider provider = shapeFactory.get(type);
        shape = Objects.requireNonNull(provider.get(), TYPE_IS_UNSUPPORTED);
    }

    public void setStroke(final StrokeTypes type) {
        StrokeProvider provider = strokeFactory.get(type);
        stroke = Objects.requireNonNull(provider.get(), TYPE_IS_UNSUPPORTED);
    }

    public void setColor(final ColorTypes type) {
        ColorProvider provider = colorFactory.get(type);
        paint = Objects.requireNonNull(provider.get(), TYPE_IS_UNSUPPORTED);
    }

    public void setWidth(final int width) {
        this.width = width;
    }

    public void setHeight(final int height) {
        this.height = height;
    }

    public Shape getShape() {
        return shape;
    }

    public BasicStroke getStroke() {
        return stroke;
    }

    public Paint getPaint() {
        return paint;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Функция создает на основе заданных параметров правильный n-угольник
     * 
     * @param arms   Число двойных ребер
     * @param center Точка, в которой находится центр
     * @param rOuter Внешний радиус
     * @param rInner Внутренний радиус
     * @return
     */
    private static Shape createStar(final int arms, final Point center,
            final double rOuter, final double rInner) {
        final double angle = 3.141592653589793 / arms;
        final GeneralPath path = new GeneralPath();
        for (int i = 0; i < 2 * arms; ++i) {
            final double r = ((i & 1) == 0) ? rOuter : rInner;
            final Point2D.Double p = new Point2D.Double(
                    center.x + Math.cos(i * angle) * r,
                    center.y + Math.sin(i * angle) * r);
            if (i == 0) {
                path.moveTo(p.getX(), p.getY());
            } else {
                path.lineTo(p.getX(), p.getY());
            }
        }
        path.closePath();
        return path;
    }

    private static Shape createTriangle(final int height, final int width) {
        final GeneralPath path = new GeneralPath();
        final double tmp_height = Math.sqrt(2.0) / 2.0 * height;
        path.moveTo(-width / 2.0, -tmp_height);
        path.lineTo(0.0, -tmp_height);
        path.lineTo(width / 2.0, tmp_height);
        path.closePath();
        return path;
    }

    @FunctionalInterface
    private interface ShapeProvider {
        Shape get();
    }

    @FunctionalInterface
    private interface StrokeProvider {
        BasicStroke get();
    }

    @FunctionalInterface
    private interface ColorProvider {
        Paint get();
    }
}
