package sumdu;
import javax.swing.SwingUtilities;

import types.ColorTypes;
import types.ShapeTypes;
import types.StrokeTypes;

import javax.swing.JFrame;

// 
// Decompiled by Procyon v0.5.36
// 

public class TitlesFrame extends JFrame
{
    private static final long serialVersionUID = 1L;

    /**
     * Конструктор главного окна,
     * который его же и отображает
     */
    public TitlesFrame() {
        initUI();
    }
    
    private void initUI() {
        setTitle("Кривые фигуры");
        setDefaultCloseOperation(3);
        
        Figure figure = new Figure();
        figure.setShape(ShapeTypes.HEXAGON);
        figure.setColor(ColorTypes.BLACK);
        figure.setStroke(StrokeTypes.THIN);
        
        add(new TitlesPanel(figure));
        setSize(350, 350);
        setLocationRelativeTo(null);
    }
    
    /**
     * Главная функция программы
     * @param args Параметры командной строки
     */
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(() -> new TitlesFrame().setVisible(true));
    }
}
