package sumdu;

import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import javax.swing.Timer;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

// 
// Decompiled by Procyon v0.5.36
// 

public class TitlesPanel extends JPanel implements ActionListener {
    private static final long serialVersionUID = 1L;
    private Timer animation;
    private transient FigureCanvas figureCanvas;

    public TitlesPanel(Figure figure) {
        figureCanvas = new FigureCanvas(figure);

        animation = new Timer(50, this);
        animation.setInitialDelay(50);
        animation.start();
    }

    @Override
    public void actionPerformed(final ActionEvent arg0) {
        if (figureCanvas.isDone()) {
            repaint();
        }
    }

    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        final Dimension size = getSize();
        final Insets insets = getInsets();
        final int w = size.width - insets.left - insets.right;
        final int h = size.height - insets.top - insets.bottom;

        figureCanvas.setGraphics2d(g2d);
        figureCanvas.setHeight(h);
        figureCanvas.setWidth(w);
        figureCanvas.draw();
    }
}
